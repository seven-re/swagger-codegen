# Swagger Codegen

This repository contains api-templates uesd for generating code for server and client

### Requirements
- `JAVA-JDK` installed
- `Git` installed

### Helpful links?
* [swagger-codegen](https://github.com/swagger-api/swagger-codegen) code for swagger-codegen
* [online-editor](http://editor.swagger.io/) online editor for generating swagger yaml files
* [getting-started] (https://github.com/swagger-api/swagger-codegen#getting-started) Basic guideliens for starting

## Usage
Generating any code requires following parameters
- `i` `<<template-api>>.json` file
- `l` language (platform specific language or framework)
- `o` output path
 

```bash
java -jar swagger-codegen-cli-2.2.1.jar generate -i config.json -l jaxrs -o myapi/server/java
```

Code for driver-api for nodejs

```bash
java -jar swagger-codegen-cli-2.2.1.jar generate -i driver-api.json -l nodejs -o myapi/server/java
```

Code for driver-api for Android with retrofit library

```bash
java -jar swagger-codegen-cli-2.2.1.jar  generate -i config.json -l jaxrs -o myapi/client/java --library retrofit
```

## Start
- Clone repository
- Edit `.json` file
- generate code using above example (set path in other directory or add generated directory to .gitingore file)
- git commit


## License
MIT
